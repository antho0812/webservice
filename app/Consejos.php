<?php

namespace controlFinanzas;

use Illuminate\Database\Eloquent\Model;

class Consejos extends Model
{
    protected $table = 'consejo';
    protected $fillable = array('estado','descripcion');
}
