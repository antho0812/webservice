<?php

namespace controlFinanzas\Http\Controllers;

use Illuminate\Http\Request;
use controlFinanzas\Helpers\APIHelpers;
use controlFinanzas\Categorias;

class CategoriasController extends Controller
{
    public function getAll(){
        $categorias= Categorias::all();
        try{

            $response = APIHelpers::createAPIResponse(false,0000,'Si Se Encontraron Categorias',$categorias);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Categorias',null);
            return response()->json($response, 200);
        }
        
    }
    public function getAllbyUser($id){
        $allCategorias = Categorias::getAllCat($id);

        try{
            $response = APIHelpers::createAPIResponse(false,0000,'Si Se Encontraron Categorias',$allCategorias);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Categorias',null);
            return response()->json($response, 200);
        }
    }

    public function getAcumulado($idUser,$mes,$anio){
        $acumulado = Categorias::getAcumulado($idUser,$mes,$anio);
        try{
            $response = APIHelpers::createAPIResponse(false,0000,'Si Se Encontraron Resultados',$acumulado);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Resultados',null);
            return response()->json($response, 200);
        }
    }
}
