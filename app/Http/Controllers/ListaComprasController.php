<?php

namespace controlFinanzas\Http\Controllers;

use Illuminate\Http\Request;
use controlFinanzas\Helpers\APIHelpers;
use controlFinanzas\ListaCompras;

class ListaComprasController extends Controller
{
    public function getAll(){
        $listaCompras= ListaCompras::all();
        try{
            $response = APIHelpers::createAPIResponse(false,0000,'Si Se Encontraron Listas',$listaCompras);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Listas',null);
            return response()->json($response, 200);
        }
        
    }

    public function add(Request $request){
            try{
                $listaCompras= ListaCompras::create($request->all());
                $idCreado = $listaCompras->id;
                $response = APIHelpers::createAPIResponse(false,0000,'Lista de Compras Registrada con Exito',$listaCompras);
                return response()->json($response, 200); 
            }catch(\Exception $e){
                $response = APIHelpers::createAPIResponse(true,9999,'No Se Pudo Registrar La Lista de Compras',null);
                return response()->json($e, 200);
            }
        
    }

    public function getByUserId($id){
        $listaCompras = ListaCompras::buscarByIdUsuario($id);
        try{
            $idCreado = $listaCompras->id;
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontraron Listas',$listaCompras);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Listas Para Ese Usuario',null);
            return response()->json($response, 200);
        }
    }

    public function get($id){
        $listaCompras = ListaCompras::find($id);
        try{
            $idCreado = $listaCompras->id;
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontro La Lista de Compras',$listaCompras);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existe esa Lista de Compras',null);
            return response()->json($response, 200);
        }
    }

    public function getByListasUserIdByAnioByMes($idUser,$anio,$mes){
        $listaCompras = ListaCompras::ListasByUserIdByAnioByMes($idUser, $anio, $mes);
        try{
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontro La Lista de Compras',$listaCompras);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existe esa Lista de Compras',null);
            return response()->json($response, 200);
        }
    }

    public function getLista($id){
        $listaCompras = ListaCompras::find($id);
        return $listaCompras;
    }

    public function edit(Request $request){
        try{
            $idLista = $request->input('id');
            $listaCompras = $this->getLista($idLista);
            $listaCompras->fill($request->all())->save();
            $response = APIHelpers::createAPIResponse(false,0000,'Se Actualizo la Lista de Compras',$listaCompras);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Se pudo Actualizar la Lista de Compras',null);
            return response()->json($response, 200);
        }
    }
}
