<?php

namespace controlFinanzas\Http\Controllers;

use Illuminate\Http\Request;
use controlFinanzas\Helpers\APIHelpers;
use controlFinanzas\Consejos;

class ConsejosController extends Controller
{
    public function getAll(){
        $consejos= Consejos::all();
        try{
            
            $response = APIHelpers::createAPIResponse(false,0000,'Si Se Encontraron Consejos',$consejos);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Consejos',null);
            return response()->json($response, 200);
        }
        
    }
}
