<?php

namespace controlFinanzas\Http\Controllers;

use Illuminate\Http\Request;
use controlFinanzas\Helpers\APIHelpers;
use controlFinanzas\CategoriasUsuario;

class CategoriasUsuarioController extends Controller
{
    public function add(Request $request){
            try{
                $categoriaUser= CategoriasUsuario::create($request->all());
                $idCreado = $categoriaUser->id;
                $response = APIHelpers::createAPIResponse(false,0000,'Categoria de Usuario Registrada con Exito',$categoriaUser);
                return response()->json($response, 200); 
            }catch(\Exception $e){
                $response = APIHelpers::createAPIResponse(true,9999,'No Se Pudo Registrar La Categoria del Usuario',null);
                return response()->json($e, 200);
            }
        
    }

    public function getByIdUsuario($id){
        $categoriaUser = CategoriasUsuario::buscarByIdUsuario($id);
        try{
            $idCreado = $categoriaUser->id;
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontraron Categorias de Usuario',$categoriaUser);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Categorias de Usuario',null);
            return response()->json($response, 200);
        }
    }

    public function get($id){
        $categoriaUser = CategoriasUsuario::find($id);
        try{
            $idCreado = $categoriaUser->id;
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontro La Categoria de Usuario',$categoriaUser);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existe esa Categoria de Usuario',null);
            return response()->json($response, 200);
        }
    }

    public function getCategoria($id){
        $categoriaUser = CategoriasUsuario::find($id);
        return $categoriaUser;
    }

    public function edit(Request $request){
        try{
            $idUsuario = $request->input('id');
            $categoriaUser = $this->getCategoria($idUsuario);
            $categoriaUser->fill($request->all())->save();
            $response = APIHelpers::createAPIResponse(false,0000,'Se Actualizo La Categoria del Usuario',$categoriaUser);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Se pudo Actualizar Del Usuario',null);
            return response()->json($response, 200);
        }
    }
}
