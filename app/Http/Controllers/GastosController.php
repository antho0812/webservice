<?php

namespace controlFinanzas\Http\Controllers;

use Illuminate\Http\Request;
use controlFinanzas\Helpers\APIHelpers;
use controlFinanzas\Gastos;
use controlFinanzas\CategoriasUsuario;

class GastosController extends Controller
{
    public function getAll(){
        $gastos= Gastos::all();
        try{
            
            $response = APIHelpers::createAPIResponse(false,0000,'Si Se Encontraron Gastos',$gastos);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Gastos',null);
            return response()->json($response, 200);
        }
        
    }

    public function add(Request $request){
            try{    
                $nuevaCategoria = $request->input('nuevaCategoria');
                if($nuevaCategoria=="true"){
                    $jsNuevaCateogira=[
                        'estado' => $request->input('estado'),
                        'id_usuario' => $request->input('id_usuario'),
                        'descripcion' => $request->input('descripcion'),
						'ucat_url_imagen' => '/img/cat_usuario.png'
                    ];
                    $nuevaC=CategoriasUsuario::create($jsNuevaCateogira);
                    if($nuevaC){
                        $jsNuevoGasto=[
                            'id_usuario' => $request->input('id_usuario'),
                            'id_ucategoria' => $nuevaC->id,
                            'estado' => $request->input('estado'),
                            'descripcion' => $request->input('descripcion'),
                            'monto' => $request->input('monto'),
                            'fecha_gasto' => $request->input('fecha_gasto')
                        ];
                        $gastos= Gastos::create($jsNuevoGasto);
                        $idCreado = $gastos->id;
                        $response = APIHelpers::createAPIResponse(false,0000,'Gasto Registrado con Exito',$gastos);
                        return response()->json($response, 200); 
                    }
                }else{
                    $gastos= Gastos::create($request->except('nuevaCategoria'));
                    $idCreado = $gastos->id;
                    $response = APIHelpers::createAPIResponse(false,0000,'Gasto Registrado con Exito',$gastos);
                    return response()->json($response, 200); 
                }
                
            }catch(\Exception $e){
                $response = APIHelpers::createAPIResponse(true,9999,'No Se Pudo Registrar El Gasto',null);
                return response()->json($response, 200);
                
            }
        
    }

    public function get($id){
        $gastos = Gastos::find($id);
        try{
            $idCreado = $gastos->id;
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontro el Gasto',$gastos);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existe ese Gasto',null);
            return response()->json($response, 200);
        }
    }

    public function getByCategoriaUser($id){
        $gastos = Gastos::buscarByIdUsuario($id);
        try{
            $idCreado = $gastos->id;
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontraron Listas',$gastos);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Listas Para Ese Usuario',null);
            return response()->json($response, 200);
        }
    }
    public function getBuscarByMes($idUsuario,$month,$year, $orden){
        $gastos = Gastos::buscarByMes($idUsuario,$month,$year, $orden);
        try{
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontraron Listas',$gastos);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Listas Para Ese Usuario',null);
            return response()->json($response, 200);
        }
    }
    public function getGasto($id){
        $gastos = Gastos::find($id);
        return $gastos;
    }

    public function edit(Request $request){
        try{
            $idGasto = $request->input('id');
            $gastos = $this->getGasto($idGasto);
            $gastos->fill($request->all())->save();
            $response = APIHelpers::createAPIResponse(false,0000,'Se Actualizo el Registro del Gasto',$gastos);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Se pudo Actualizar el Registro de Gasto',null);
            return response()->json($response, 200);
        }
    }
    public function buscarByUserByCategoriaByMesByAnio($idUsuario,$categoria,$month,$year){
        $gastos = Gastos::buscarByUserByCategoriaByMesByAnio($idUsuario,$categoria,$month,$year);
        try{
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontraron Listas',$gastos);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Listas Para Ese Usuario',null);
            return response()->json($response, 200);
        }
    }
}
