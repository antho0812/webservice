<?php

namespace controlFinanzas\Http\Controllers;

use Illuminate\Http\Request;
use controlFinanzas\Helpers\APIHelpers;
use controlFinanzas\DetalleCompras;

class DetalleComprasController extends Controller
{
    public function getAll(){
        $detalleCompras= DetalleCompras::all();
        try{
            $response = APIHelpers::createAPIResponse(false,0000,'Si Se Encontraron Detalles',$detalleCompras);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Detalles',null);
            return response()->json($response, 200);
        }
        
    }

    public function add(Request $request){
            try{
                $detalleCompras= DetalleCompras::create($request->all());
                $idCreado = $detalleCompras->id;
                $response = APIHelpers::createAPIResponse(false,0000,'Detalle de Compra Registrado con Exito',$detalleCompras);
                return response()->json($response, 200); 
            }catch(\Exception $e){
                $response = APIHelpers::createAPIResponse(true,9999,'No Se Pudo Registrar El Detalle de la Compra',null);
                return response()->json($response, 200);
            }
        
    }

    public function getByIdLista($id){
        $detalleCompras = DetalleCompras::buscarByIdLista($id);
        try{
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontraron Detalles de la Compra',$detalleCompras);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Detalles para la Compra',null);
            return response()->json($response, 200);
        }
    }

    public function get($id){
        $detalleCompras = DetalleCompras::find($id);
        try{
            $idCreado = $detalleCompras->id;
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontro El Detalle de la Compra',$detalleCompras);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existe ese Detalle de Compra',null);
            return response()->json($response, 200);
        }
    }

    public function getLista($id){
        $detalleCompras = DetalleCompras::find($id);
        return $detalleCompras;
    }

    public function edit(Request $request){
        try{
            $idDetalle = $request->input('id');
            $detalleCompras = $this->getLista($idDetalle);
            $detalleCompras->fill($request->all())->save();
            $response = APIHelpers::createAPIResponse(false,0000,'Se Actualizo el Detalle de Compra',$detalleCompras);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Se pudo Actualizar El Detalle de Compra',null);
            return response()->json($response, 200);
        }
    }
}
