<?php

namespace controlFinanzas\Http\Controllers;

use Illuminate\Http\Request;
use controlFinanzas\Helpers\APIHelpers;
use controlFinanzas\Usuarios;

class UsuariosController extends Controller
{
    //

    public function validateUser(Request $request){
        $bodyContent = $request->getContent();
        $tipo_login= json_decode($bodyContent)->tipo_login;
        if($tipo_login == 'up'){
            $correo = $request->input('correo');
            $Usuarios = Usuarios::buscarCorreo($correo);
                try{
                    $correoEnviado = $Usuarios->correo;
                    $response = APIHelpers::createAPIResponse(false,0000,'Usuario Encontrado',$Usuarios);
                    return response()->json($response, 200);

                }catch(\Exception $e){
                    $response = APIHelpers::createAPIResponse(true,9999,'Usuario No Existe',null);
                    return response()->json($response, 200);

                }
                
           
            
        }else{
            $token = $request->input('token');
            $Usuarios = Usuarios::buscarToken($token);
            try{
                $tokenEnviado = $Usuarios->token;
                $response = APIHelpers::createAPIResponse(false,0000,'Usuario Encontrado',$Usuarios);
                return response()->json($response, 200);

            }catch(\Exception $e){
                $response = APIHelpers::createAPIResponse(true,9999,'Usuario No Existe',null);
                return response()->json($response, 200);

            }

        }
        
    }
    public function validateLogin(Request $request){
        $correo = $request->input('correo');
        $password = $request->input('password');
        $token = $request->input('token');
        $Usuarios = null;

        if($token){
            $Usuarios = Usuarios::buscarToken($token);
        }else{
            $Usuarios = Usuarios::validarLogeo($correo,$password);
        }

        try{
            $correoEnviado = $Usuarios->correo;
            $response = APIHelpers::createAPIResponse(false,0000,'Login Exitoso',$Usuarios);
            return response()->json($response, 200);
        }catch(\Exception $e){
            if($token){
                $response = APIHelpers::createAPIResponse(true,9999,'La cuenta seleccionada no esta regstrada.',null);
            }else{
                $response = APIHelpers::createAPIResponse(true,9999,'Credenciales Incorrectas',null);
            }
            return response()->json($response, 200);
        }
    }
    /* public function validateLogin(Request $request){
        $correo = $request->input('correo');
        $password = $request->input('password');
        $Usuarios = Usuarios::validarLogeo($correo,$password);
        try{
            $correoEnviado = $Usuarios->correo;
            $response = APIHelpers::createAPIResponse(false,0000,'Login Exitoso',$Usuarios);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'Credenciales Incorrectas',null);
            return response()->json($response, 200);
        }


    } */

    public function getAll(){
        $usuarios= Usuarios::all();
        try{
            
            $response = APIHelpers::createAPIResponse(false,0000,'Si Se Encontraron Usuarios',$usuarios);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existen Usuarios',null);
            return response()->json($response, 200);
        }
        
    }
    
    public function add(Request $request){
            $correo = $request->input('correo');
            $userCorreo = Usuarios::buscarCorreo($correo);
            try{
                $correoEnviado = $userCorreo->correo;
                $response = APIHelpers::createAPIResponse(true,9999,'Ya existe un registro con el Email Ingresado',null);
                return response()->json($response, 200); 

            }catch(\Exception $e){
                try{
                    $usuarios= Usuarios::create($request->all());
                    $idCreado = $usuarios->id;
                    $response = APIHelpers::createAPIResponse(false,0000,'Usuario Registrado con Exito',$usuarios);
                    return response()->json($response, 200); 
                }catch(\Exception $e){
                    $response = APIHelpers::createAPIResponse(true,9999,'No Se Pudo Registrar El Usuario',null);
                    return response()->json($response, 200);
                }
            }
    }

    public function get($id){
        $usuarios = Usuarios::find($id);
        try{
            $idCreado = $usuarios->id;
            $response = APIHelpers::createAPIResponse(false,0000,'Se Encontro el Usuario',$usuarios);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Existe ese Usuario',null);
            return response()->json($response, 200);
        }
    }
    
    public function getUser($id){
        $usuarios = Usuarios::find($id);
        return $usuarios;
    }
    

    public function edit(Request $request){
        try{
            $idUser = $request->input('id');
            $usuarios = $this->getUser($idUser);
            $usuarios->fill($request->all())->save();
            $response = APIHelpers::createAPIResponse(false,0000,'Se Actualizo el Registro del Usuario',$usuarios);
            return response()->json($response, 200);
        }catch(\Exception $e){
            $response = APIHelpers::createAPIResponse(true,9999,'No Se pudo Actualizar el Usuario',null);
            return response()->json($response, 200);
        }
    }
    public function pruebaswift(Request $request){
        return $request->getContent();
    }
}
