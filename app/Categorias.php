<?php

namespace controlFinanzas;

use Illuminate\Database\Eloquent\Model;
use controlFinanzas\CategoriasUsuario;


class Categorias extends Model
{
    protected $table = 'categoria';
    protected $fillable = array('estado','descripcion','url_imagen');

    /* public function scopeGetAllCat($query,$id_usuario){
        if($id_usuario){
            $first = Categorias::select('descripcion');
            return CategoriasUsuario::select('descripcion')->where('id_usuario',$id_usuario)->union($first)->get();
            }
            
        } */
        public function scopeGetAllCat($query,$id_usuario){
            if($id_usuario){
                $first = Categorias::selectRaw("id, NULL  as idUsuarioCategoria, descripcion, cat_url_imagen as url_imagen");
                return CategoriasUsuario::selectRaw("NULL as id, id as idUsuarioCategoria, descripcion, ucat_url_imagen as url_imagen")->where('id_usuario',$id_usuario)->union($first)->get();
            }
                
        }
        public function scopeGetAcumulado($query,$id_usuario,$mes,$anio){
            if($id_usuario){
                $first = Categorias::selectRaw('NULL as id_ucategoria, g.id_categoria, c.descripcion, sum(g.monto) as total, max(g.fecha_gasto) as ultimaEntrada, c.cat_url_imagen as url_imagen')->from('categoria as c')->join('gasto as g', 'c.id', '=', 'g.id_categoria')->where('g.id_usuario',$id_usuario)->where('g.estado',"1")->whereMonth('g.fecha_gasto',$mes)->whereYear('g.fecha_gasto',$anio)->groupBy('g.id_categoria','c.descripcion','c.cat_url_imagen')->get();
                
                $second = CategoriasUsuario::selectRaw('g.id_ucategoria, NULL as id_categoria, uc.descripcion, sum(g.monto) as total, max(g.fecha_gasto) as ultimaEntrada, uc.ucat_url_imagen as url_imagen')->from('usuario_categoria as uc')->join('gasto as g','uc.id','=','g.id_ucategoria')->where('g.id_usuario',$id_usuario)->where('g.estado',"1")->whereMonth('g.fecha_gasto',$mes)->whereYear('g.fecha_gasto',$anio)->groupBy('g.id_ucategoria','uc.descripcion','uc.ucat_url_imagen')->get();
                return array_merge($first->toArray(),$second->toArray());
            }
        }
}
