<?php

namespace controlFinanzas;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    protected $table = 'usuario';
    protected $fillable = array('estado','correo','password','token','tipo_login','nombres','apellidos','edad','genero');


    public function scopeBuscarCorreo($query,$correo){
        if($correo){
            return Usuarios::where('correo',$correo)->where('estado','1')->first();
        }
    }

    public function scopeBuscarToken($query,$token){
        if($token){
            return Usuarios::where('token',$token)->where('estado','1')->first();
        }
    }

    public function scopeValidarLogeo($query,$correo,$password){
        if($correo){
            return Usuarios::where('correo',$correo)->where('password',$password)->first();
        }
    }
}
