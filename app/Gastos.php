<?php

namespace controlFinanzas;

use Illuminate\Database\Eloquent\Model;


class Gastos extends Model
{
    protected $table = 'gasto';
    protected $fillable = array('id_categoria','id_ucategoria','estado','descripcion','monto','fecha_gasto','id_usuario');

    public function scopeBuscarByIdUsuario($query,$id_ucategoria){
        if($id_ucategoria){
            return Gastos::where('id_ucategoria',$id_ucategoria)->where('estado','1');
        }
    }
    /* public function scopeBuscarByMes($query,$id_usuario,$month,$year){
        if($id_usuario){
            return Gastos::whereMonth('fecha_gasto',$month)->whereYear('fecha_gasto',$year)->where('id_usuario',$id_usuario)->where('estado','1')->get();
        }
    } */

    public function scopeBuscarByMes($query,$id_usuario,$month,$year, $orden){
        if($id_usuario){
            return Gastos::select('gasto.id','gasto.id_categoria','categoria.descripcion as desc_cat','categoria.cat_url_imagen','gasto.id_ucategoria','usuario_categoria.descripcion as desc_ucat','usuario_categoria.ucat_url_imagen','gasto.descripcion','gasto.monto','gasto.fecha_gasto')->from('categoria')->rightJoin('gasto', 'gasto.id_categoria', '=', 'categoria.id')->leftJoin('usuario_categoria', 'usuario_categoria.id', '=', 'gasto.id_ucategoria')->whereMonth('gasto.fecha_gasto',$month)->whereYear('gasto.fecha_gasto',$year)->where('gasto.id_usuario',$id_usuario)->where('gasto.estado','1')->orderBy('fecha_gasto',$orden)->get();
        }
    }

    public function scopeBuscarByUserByCategoriaByMesByAnio($query,$id_usuario,$categoria,$month,$year){
        if($id_usuario){
            return Gastos::select('gasto.id','gasto.id_categoria','categoria.descripcion as desc_cat','categoria.cat_url_imagen','gasto.id_ucategoria','usuario_categoria.descripcion as desc_ucat','usuario_categoria.ucat_url_imagen','gasto.descripcion','gasto.monto','gasto.fecha_gasto')->from('categoria')->rightJoin('gasto', 'gasto.id_categoria', '=', 'categoria.id')->leftJoin('usuario_categoria', 'usuario_categoria.id', '=', 'gasto.id_ucategoria')->whereMonth('gasto.fecha_gasto',$month)->whereYear('gasto.fecha_gasto',$year)->where('gasto.id_usuario',$id_usuario)->where('gasto.estado','1')->whereRaw('(gasto.id_categoria = '.$categoria.' or gasto.id_ucategoria = '.$categoria.')')->get();
        }
    }
}
