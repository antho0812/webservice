<?php

namespace controlFinanzas;

use Illuminate\Database\Eloquent\Model;

class ListaCompras extends Model
{
    protected $table = 'lista_compra';
    protected $fillable = array('id_usuario','estado','descripcion');

    public function scopeBuscarByIdUsuario($query,$id_usuario){
        if($id_usuario){
            return ListaCompras::where('id_usuario',$id_usuario)->where('estado','1');
        }
    }

    public function scopeListasByUserIdByAnioByMes($query,$idUser,$anio,$mes){
            return ListaCompras::select("lista_compra.*")->selectRaw("sum(detalle_lista.monto) as total, count(detalle_lista.monto) as total_items")->leftJoin('detalle_lista', 'detalle_lista.id_lista', '=', 'lista_compra.id')->where('id_usuario',$idUser)->where('lista_compra.estado','1')->whereMonth("lista_compra.created_at", $mes)->whereYear("lista_compra.created_at",$anio)->groupBy("lista_compra.id","lista_compra.id_usuario","lista_compra.estado", "lista_compra.descripcion","lista_compra.created_at","lista_compra.updated_at")->get();
    }
}
