<?php

namespace controlFinanzas;

use Illuminate\Database\Eloquent\Model;

class CategoriasUsuario extends Model
{
    protected $table = 'usuario_categoria';
    protected $fillable = array('id_usuario','estado','descripcion', 'ucat_url_imagen');

    public function scopeBuscarByIdUsuario($query,$id_usuario){
        if($id_usuario){
            return CategoriasUsuario::where('id_usuario',$id_usuario)->where('estado','1');
        }
    }
}
