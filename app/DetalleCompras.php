<?php

namespace controlFinanzas;

use Illuminate\Database\Eloquent\Model;

class DetalleCompras extends Model
{
    protected $table = 'detalle_lista';
    protected $fillable = array('id_lista','estado','descripcion','monto');

    public function scopeBuscarByIdLista($query,$id_lista){
            return DetalleCompras::where('id_lista',$id_lista)->where('estado','1')->orderBy("created_at", "desc")->get();
    }
}
