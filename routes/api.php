<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::put('pruebaswift','UsuariosController@pruebaswift');

Route::post('validarUser','UsuariosController@validateUser');
Route::post('validarLogin','UsuariosController@validateLogin');

Route::get('usuario/consultar','UsuariosController@getAll');
Route::post('usuario/insertar','UsuariosController@add');
Route::get('usuario/consultar/{id}','UsuariosController@get');
Route::post('usuario/actualizar','UsuariosController@edit'); 

Route::get('gasto/consultar','GastosController@getAll');
Route::post('gasto/insertar','GastosController@add');
Route::get('gasto/consultar/{id}','GastosController@get');
Route::get('gasto/consultar2/{id}','GastosController@getByCategoriaUser');
Route::get('gasto/consultar3/{idUsuario}/{month}/{year}/{order}','GastosController@getBuscarByMes');
Route::post('gasto/actualizar','GastosController@edit');
Route::get('gasto/gastosusuario/{idUsuario}/{categoria}/{month}/{year}','GastosController@buscarByUserByCategoriaByMesByAnio');
//gastosByCategoriaUser
//gastosByCategoria

Route::get('listacompra/consultar','ListaComprasController@getAll');
Route::post('listacompra/insertar','ListaComprasController@add');
Route::get('listacompra/consultar3/{idUser}/{anio}/{mes}','ListaComprasController@getByListasUserIdByAnioByMes');
Route::get('listacompra/consultar2/{id}','ListaComprasController@getByUserId');
Route::get('listacompra/consultar/{id}','ListaComprasController@get');
Route::post('listacompra/actualizar','ListaComprasController@edit');

Route::get('detallelista/consultar','DetalleComprasController@getAll');
Route::post('detallelista/insertar','DetalleComprasController@add');
Route::get('detallelista/consultar2/{id}','DetalleComprasController@getByIdLista');
Route::get('detallelista/consultar/{id}','DetalleComprasController@get');
Route::post('detallelista/actualizar','DetalleComprasController@edit');

Route::get('consejo/consultar','ConsejosController@getAll');

Route::get('categoria/consultar','CategoriasController@getAll');
Route::get('categoria/consultar2/{id}','CategoriasController@getAllbyUser');
Route::get('categoria/acumulado/{idUser}/{mes}/{anio}','CategoriasController@getAcumulado');

Route::get('usuariocategoria/consultar','CategoriasUsuarioController@getAll');
Route::post('usuariocategoria/insertar','CategoriasUsuarioController@add');
Route::get('usuariocategoria/consultar2/{id}','CategoriasUsuarioController@getByIdUsuario');
Route::get('usuariocategoria/consultar/{id}','CategoriasUsuarioController@get');
Route::post('usuariocategoria/actualizar','CategoriasUsuarioController@edit');

