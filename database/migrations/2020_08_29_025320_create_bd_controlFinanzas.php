<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBdControlFinanzas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        //Estado
        Schema::create('estado', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('descripcion');

            $table->engine = 'InnoDB';
        });
        //Categoria
        Schema::create('categoria', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('estado')->unsigned();
            $table->string('descripcion');
            $table->string('cat_url_imagen')->nullable();

            $table->engine = 'InnoDB';
        });
        Schema::table('categoria', function (Blueprint $table) {
            $table->foreign('estado')->references('id')->on('estado');
        });
        //Consejo
        Schema::create('consejo', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('estado')->unsigned();
            $table->string('descripcion');
            $table->timestamps();

            $table->engine = 'InnoDB';

        });

        Schema::table('consejo', function (Blueprint $table) {
            $table->foreign('estado')->references('id')->on('estado');
        });

        //Usuarios
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('estado')->unsigned();
            $table->string('correo')->unique()->nullable();
            $table->string('password')->nullable();
            $table->integer('token',250)->nullable();
            $table->string('tipo_login')->nullable();
            $table->string('nombres')->nullable();
            $table->string('apellidos')->nullable();
            $table->integer('edad')->nullable();
            $table->integer('genero')->nullable();//0 mujer, 1 hombre
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::table('usuario', function (Blueprint $table) {
            $table->foreign('estado')->references('id')->on('estado');
        });

        //Usuario_Categoria
        Schema::create('usuario_categoria', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->integer('estado')->unsigned();
            $table->string('descripcion');
            $table->string('ucat_url_imagen')->nullable();
            $table->timestamps();

            $table->engine = 'InnoDB';
        });
        Schema::table('usuario_categoria', function (Blueprint $table) {
            $table->foreign('id_usuario')->references('id')->on('usuario');
            $table->foreign('estado')->references('id')->on('estado');
        });

         //Gasto
         Schema::create('gasto', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_categoria')->unsigned()->nullable();
            $table->integer('id_ucategoria')->unsigned()->nullable();
            $table->integer('estado')->unsigned();
            $table->string('descripcion');
            $table->float('monto', 8, 2);
            $table->date('fecha_gasto');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::table('gasto', function (Blueprint $table) {
            $table->foreign('id_usuario')->references('id')->on('usuario');
            $table->foreign('id_categoria')->references('id')->on('categoria');
            $table->foreign('id_ucategoria')->references('id')->on('usuario_categoria');
            $table->foreign('estado')->references('id')->on('estado');
        });

        //Lista_Compra
        Schema::create('lista_compra', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->integer('estado')->unsigned();
            $table->string('descripcion');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
        Schema::table('lista_compra', function (Blueprint $table) {
            $table->foreign('estado')->references('id')->on('estado');
            $table->foreign('id_usuario')->references('id')->on('usuario');
        });

        
        //Detalle_Lista
        Schema::create('detalle_lista', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_lista')->unsigned();
            $table->integer('estado')->unsigned();
            $table->string('descripcion');
            $table->float('monto', 8, 2);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
        Schema::table('detalle_lista', function (Blueprint $table) {
            $table->foreign('estado')->references('id')->on('estado');
            $table->foreign('id_lista')->references('id')->on('lista_compra');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Estado
        Schema::dropIfExists('estado');
        //Categoria
        Schema::dropIfExists('categoria');
        //Consejo
        Schema::dropIfExists('consejo');
        //Usuarios
        Schema::dropIfExists('usuario');
        //Usuario_Categoria
        Schema::dropIfExists('usuario_categoria');
        //Gasto
        Schema::dropIfExists('gasto');
        //Lista_Compra
        Schema::dropIfExists('lista_compra');
        //Detalle_Lista
        Schema::dropIfExists('detalle_lista');
    }
}
